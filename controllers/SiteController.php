<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\SqlDataProvider; //importante las dependencias
use yii\data\ActiveDataProvider; //importante las dependencias
use app\models\Ciclista;//importante las dependencias
use app\models\Lleva;
use app\models\Equipo;
use app\models\Puerto;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionConsulta1a(){
         //mediante Active Record 
        $dataProvider = new ActiveDataProvider([
            'query'=> Ciclista::find()->Select('nombre,dorsal')->distinct()
                
                ->where ('edad BETWEEN 25 AND 35'),
                

        ]);

                return $this->render("resultado",[
                    "resultados"=>$dataProvider,
                    "campos"=>['nombre','dorsal'],
                    "titulo"=>"Consulta 1a con Active Record",
                    "enunciado"=>"Los ciclistas con la edad entre",
                    "sql"=>"Select nombre,dorsal from ciclistas where edad BETWEEN 25 AND 35",

                ]);
    }
    
    
    public function actionConsulta3a(){
        
        //mediante Active Record
        
        $dataProvider = new ActiveDataProvider([
            'query'=> Ciclista::find()->Select('dorsal,nompuerto')->distinct()
                
                ->where ('altura > 1500'),
                

        ]);

                return $this->render("resultado",[
                    "resultados"=>$dataProvider,
                    "campos"=>['dorsal','nompuerto'],
                    "titulo"=>"Consulta 3a con Active Record",
                    "enunciado"=>"select dorsal,nompuerto from ciclistas where altura>1500",
                    "sql"=>"select dorsal,nompuerto from ciclistas where altura>1500 ",

                ]);
        
        
        
    }
    
    
    
    
    
}
